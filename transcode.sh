for i in *.avi; 
do
    printf "transcoding $i... ";
    ffmpeg -loglevel fatal -i "$i" -c:v libx264 -crf 21 -preset slow -c:a aac -ac 2 "${i%.*}.mp4";
    printf "done\n";
done
